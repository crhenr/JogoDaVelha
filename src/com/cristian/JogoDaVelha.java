package com.cristian;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URI;

/**
* Jogo da Velha simples em Java.
* @author Cristian Henrique (cristianmsbr@gmail.com)
*/

public class JogoDaVelha extends JFrame {
	private JButton[] botoes = new JButton[9];
	private JButton novoJogo, sair, zerarPlacar;
	private JPanel painelSuperior, painelCentro, painelInferior, painelScore;
	private JLabel nome, creditos, vitoriasL, vitoriasX, vitoriasO, emp;
	private String ultimaJogada = "O";
	private boolean vitoria = false, empateB = false;
	private int contador = 0;
	private int contVX, contVO, empate;

	public JogoDaVelha() {
		this.setTitle("Jogo da Velha");
		construirGUI();
	}

	private void construirGUI() {
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Exception ex) {  }

		painelCentro = new JPanel();
		painelCentro.setLayout(new GridLayout(3, 3, 5, 5));
		for (int i = 0; i < botoes.length; i++) {
			botoes[i] = new JButton();
			botoes[i].setFont(new Font("Monospaced", Font.PLAIN, 72));
			botoes[i].setText("");
			botoes[i].addActionListener(new BotaoListener());
			painelCentro.add(botoes[i]);
		}

		Box boxPlacar = new Box(BoxLayout.Y_AXIS);
		painelScore = new JPanel();
		vitoriasL = new JLabel("Vitórias");
		vitoriasL.setFont(new Font("Monospaced", Font.PLAIN, 16));
		vitoriasX =  new JLabel("Jogador X : 0");
		vitoriasO =  new JLabel("Jogador O : 0");
		emp = new JLabel("Empates  : 0");

		novoJogo = new JButton("Novo jogo");
		zerarPlacar = new JButton("Zerar");
		sair = new JButton("Sair");

		novoJogo.addActionListener(new NovoJogoListener());
		zerarPlacar.addActionListener(new ZerarPlacarListener());
		sair.addActionListener(new SairListener());

		novoJogo.setMinimumSize(new Dimension(90, 25));
		novoJogo.setPreferredSize(new Dimension(90, 25));
		novoJogo.setMaximumSize(new Dimension(90, 25));

		zerarPlacar.setMinimumSize(new Dimension(90, 25));
		zerarPlacar.setPreferredSize(new Dimension(90, 25));
		zerarPlacar.setMaximumSize(new Dimension(90, 25));

		sair.setMinimumSize(new Dimension(90, 25));
		sair.setPreferredSize(new Dimension(90, 25));
		sair.setMaximumSize(new Dimension(90, 25));

		boxPlacar.add(vitoriasL);
		boxPlacar.add(vitoriasX);
		boxPlacar.add(vitoriasO);
		boxPlacar.add(emp);
		boxPlacar.add(novoJogo);
		boxPlacar.add(zerarPlacar);
		boxPlacar.add(sair);
		painelScore.add(boxPlacar);

		nome = new JLabel("Jogo da Velha");
		nome.setFont(new Font("Monospaced", Font.PLAIN, 36));
		painelSuperior = new JPanel();
		painelSuperior.add(nome);

		creditos = new JLabel("2016 - Cristian Henrique");
		creditos.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent ev) {
				try {
					Desktop.getDesktop().browse(URI.create("https://www.github.com/cristian-henrique/"));
				} catch (Exception ex) {  }
			}
		});
		creditos.setCursor(new Cursor(Cursor.HAND_CURSOR));
		painelInferior = new JPanel();
		painelInferior.add(creditos);

		this.getContentPane().add(BorderLayout.NORTH, painelSuperior);
		this.getContentPane().add(BorderLayout.CENTER, painelCentro);
		this.getContentPane().add(BorderLayout.EAST, painelScore);
		this.getContentPane().add(BorderLayout.SOUTH, painelInferior);
		this.setResizable(false);
		this.setSize(475, 400);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}

	class BotaoListener implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			JButton btnPressionado = (JButton) ev.getSource();
			if (btnPressionado.getText().equals("")) {
				contador += 1;
				if (ultimaJogada.equals("O")) {
					btnPressionado.setText("X");
					verificarVitoria("X");
					ultimaJogada = "X";
				} else {
					btnPressionado.setText("O");
					verificarVitoria("O");
					ultimaJogada = "O";
				}

				if (vitoria == true || empateB == true) {
					for (int i = 0; i < botoes.length; i++) {
						botoes[i].setEnabled(false);
					}
				}
			}
		}
	}

	class NovoJogoListener implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			for (int i = 0; i < botoes.length; i++) {
				botoes[i].setText("");
				botoes[i].setEnabled(true);
				botoes[i].setForeground(Color.BLACK);
			}
			vitoria = false;
			empateB = false;
			contador = 0;
			ultimaJogada = "O";
		}
	}

	class ZerarPlacarListener implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			contVX = 0;
			contVO = 0;
			empate = 0;
			contador = 0;
			vitoriasX.setText("Jogador X : " + contVX);
			vitoriasO.setText("Jogador O : " + contVO);
			emp.setText("Empates  : " + empate);
		}
	}

	class SairListener implements ActionListener {
		public void actionPerformed(ActionEvent ev) {
			System.exit(0);
		}
	}

	private void pintarForeground(int i1, int i2, int i3) {
		botoes[i1].setForeground(Color.RED);
		botoes[i2].setForeground(Color.RED);
		botoes[i3].setForeground(Color.RED);
		vitoria = true;
	}

	private void verificarVitoria(String jog) {
		if (botoes[0].getText().equals(jog) && botoes[1].getText().equals(jog) && botoes[2].getText().equals(jog)) {
			pintarForeground(0, 1, 2);
		}

		if (botoes[3].getText().equals(jog) && botoes[4].getText().equals(jog) && botoes[5].getText().equals(jog)) {
			pintarForeground(3, 4, 5);
		}

		if (botoes[6].getText().equals(jog) && botoes[7].getText().equals(jog) && botoes[8].getText().equals(jog)) {
			pintarForeground(6, 7, 8);
		}

		if (botoes[0].getText().equals(jog) && botoes[3].getText().equals(jog) && botoes[6].getText().equals(jog)) {
			pintarForeground(0, 3, 6);
		}

		if (botoes[1].getText().equals(jog) && botoes[4].getText().equals(jog) && botoes[7].getText().equals(jog)) {
			pintarForeground(1, 4, 7);
		}

		if (botoes[2].getText().equals(jog) && botoes[5].getText().equals(jog) && botoes[8].getText().equals(jog)) {
			pintarForeground(2, 5, 8);
		}

		if (botoes[0].getText().equals(jog) && botoes[4].getText().equals(jog) && botoes[8].getText().equals(jog)) {
			pintarForeground(0, 4, 8);
		}

		if (botoes[2].getText().equals(jog) && botoes[4].getText().equals(jog) && botoes[6].getText().equals(jog)) {
			pintarForeground(2, 4, 6);
		}

		if (jog.equals("X") && vitoria == true) {
			JOptionPane.showMessageDialog(null, "O Jogador X venceu!");
			contVX += 1;
			vitoriasX.setText("Jogador X : " + contVX);
		} else if (jog.equals("O") && vitoria == true) {
			JOptionPane.showMessageDialog(null, "O Jogador O venceu!");
			contVO += 1;
			vitoriasO.setText("Jogador O : " + contVO);
		} else if (vitoria == false && contador == 9) {
			JOptionPane.showMessageDialog(null, "Empate!");
			empate += 1;
			empateB = true;
			emp.setText("Empates  : " + empate);
		}
	}

	public static void main (String[] args) {
		new JogoDaVelha();
	}
}